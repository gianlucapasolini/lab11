﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_JavaToCSharp.robot.Base
{
    public class BaseRobot : IRobot
    {
        public const double BATTERY_FULL = 100;
        public const double MOVEMENT_DELTA_CONSUMPTION = 1.2;
        private const int MOVEMENT_DELTA = 1;

        public double BatteryLevel { get; private set; }
        public RobotEnvironment Environment { get; private set; }
        public string RobotName { get; private set; }

        public BaseRobot(string robotName)
        {
            this.Environment = new RobotEnvironment(new RobotPosition(0, 0));
            this.RobotName = robotName;
            this.BatteryLevel = BATTERY_FULL;
        }

        protected void ConsumeBattery(double amount)
        {
            if (BatteryLevel >= amount)
            {
                this.BatteryLevel -= amount;
            }
            else
            {
                this.BatteryLevel = 0;
            }
        }

        private void ConsumeBatteryForMovement()
        {
            ConsumeBattery(GetBatteryRequirementForMovement());
        }

        protected double GetBatteryRequirementForMovement()
        {
            return MOVEMENT_DELTA_CONSUMPTION;
        }

        public double GetBatteryLevel()
        {
            return Math.Round(BatteryLevel * 100d) / BATTERY_FULL;
        }

        public IPosition2D GetPosition()
        {
            return this.Environment.Position;
        }

        protected bool IsBatteryEnough(double operationCost)
        {
            return this.BatteryLevel > operationCost;
        }

        protected void Log(string msg)
        {
            Console.WriteLine("[" + this.RobotName + "]: " + msg);
        }

        private bool Move(int dx, int dy)
        {
            if (IsBatteryEnough(GetBatteryRequirementForMovement()))
            {
                if (this.Environment.Move(dx, dy))
                {
                    ConsumeBatteryForMovement();
                    Log("Moved to position " + this.Environment.Position + ". Battery: " + GetBatteryLevel() + "%.");
                    return true;
                }
                Log("Can not move of (" + dx + "," + dy
                        + ") the robot is touching the world boundary: current position is " + this.Environment.Position);
            }
            else
            {
                Log("Can not move, not enough battery. Required: " + GetBatteryRequirementForMovement()
                    + ", available: " + this.BatteryLevel + " (" + GetBatteryLevel() + "%)");
            }
            return false;
        }
        public bool MoveDown()
        {
            return Move(0, -MOVEMENT_DELTA);
        }
        public bool MoveLeft()
        {
            return Move(-MOVEMENT_DELTA, 0);
        }
        public bool MoveRight()
        {
            return Move(MOVEMENT_DELTA, 0);
        }
        public bool MoveUp()
        {
            return Move(0, MOVEMENT_DELTA);
        }
        public void Recharge()
        {
            this.BatteryLevel = BATTERY_FULL;
        }
        public override string ToString()
        {
            return this.RobotName;
        }
    }
}
