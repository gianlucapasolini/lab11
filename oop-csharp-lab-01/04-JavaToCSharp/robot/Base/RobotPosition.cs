﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_JavaToCSharp.robot.Base
{
    public class RobotPosition : IPosition2D
    {
        public int X { get; private set; }
        public int Y { get; private set; }

        public int GetX()
        {
            return X;
        }

        public int GetY()
        {
            return Y;
        }

        public RobotPosition(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }

        public override bool Equals(Object o)
        {
            if (o is IPosition2D p)
            {
                return X == p.GetX() && Y == p.GetY();
            }
            return false;
        }

        public override int GetHashCode()
        {
            /*
             * This could be implemented WAY better.
             */
            return X ^ Y;
        }

        public IPosition2D SumVector(IPosition2D p)
        {
            return new RobotPosition(X + p.GetX(), Y + p.GetY());
        }

        public IPosition2D SumVector(int x, int y)
        {
            return new RobotPosition(this.X + x, this.Y + y);
        }

        public override string ToString()
        {
            return "[" + X + ", " + Y + "]";
        }
    }
}
