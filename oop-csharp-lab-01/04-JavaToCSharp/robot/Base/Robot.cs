﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_JavaToCSharp.robot.Base
{
    public interface IRobot
    {
        bool MoveUp();

        bool MoveRight();

        void Recharge();

        double GetBatteryLevel();

        IPosition2D GetPosition();
    }
}
