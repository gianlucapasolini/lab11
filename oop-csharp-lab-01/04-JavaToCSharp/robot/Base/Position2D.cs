﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_JavaToCSharp.robot.Base
{
    public interface IPosition2D
    {
        int GetX();

        int GetY();

        IPosition2D SumVector(IPosition2D p);

        IPosition2D SumVector(int x, int y);
    }
}
