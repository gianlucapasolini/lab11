﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_JavaToCSharp.robot.Base
{
    public class RobotEnvironment
    {
        public const int X_UPPER_LIMIT = 50;
        public const int X_LOWER_LIMIT = 0;
        public const int Y_UPPER_LIMIT = 80;
        public const int Y_LOWER_LIMIT = 0;
        public IPosition2D Position { get; private set; }

        public RobotEnvironment(RobotPosition position)
        {
            this.Position = position;
        }

        protected bool IsWithinWorld(IPosition2D p)
        {
            var x = p.GetX();
            var y = p.GetY();
            return x >= X_LOWER_LIMIT && x <= X_UPPER_LIMIT && y >= Y_LOWER_LIMIT && y <= Y_UPPER_LIMIT;
        }

        public bool Move(int dx, int dy)
        {
            var newPos = this.Position.SumVector(dx, dy);
            if (IsWithinWorld(newPos))
            {
                this.Position = newPos;
                return true;
            }
            return false;
        }
    }
}
