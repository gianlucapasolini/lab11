﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_JavaToCSharp.robot.composable
{
    public interface IComposableRobot : Base.IRobot
    {

        void AttachComponent(components.IRobotPart rp);

        void DetachComponent(components.IRobotPart rp);

        void DoCycle();

    }
}
