﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_JavaToCSharp.robot.components
{
    public abstract class AbstractPart : IRobotPart
    {
        public bool On { get; private set; }
        public string Description { get; private set; }
        public double EnergyRequired { get; private set; }
        protected composable.IComposableRobot Robot { get; set; }

        protected AbstractPart(string desc, double consumption)
        {
            Description = desc;
            EnergyRequired = consumption;
        }

        public string GetName()
        {
            return Description;
        }

        public bool IsOn()
        {
            return On;
        }

        public void TurnOff()
        {
            On = false;
        }

        public void TurnOn()
        {
            On = true;
        }

        public void Plug(composable.IComposableRobot cr)
        {
            Unplug();
            Robot = cr;
        }

        public void Unplug()
        {
            TurnOff();
            Robot = null;
        }

        public bool IsPlugged()
        {
            return Robot != null;
        }

        public bool IsPluggedTo(Base.IRobot r)
        {
            return IsPlugged() && Robot.Equals(r);
        }

        public double GetEnergyRequired()
        {
            return EnergyRequired;
        }

        public override abstract string ToString();

        public abstract bool DoOperation();
    }
}
