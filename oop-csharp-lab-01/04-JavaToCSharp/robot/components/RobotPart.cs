﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_JavaToCSharp.robot.components
{
    public interface IRobotPart
    {
        bool DoOperation();

        double GetEnergyRequired();

        string GetName();

        bool IsOn();

        bool IsPlugged();

        bool IsPluggedTo(Base.IRobot cr);

        void Plug(composable.IComposableRobot cr);

        void TurnOff();

        void TurnOn();

        void Unplug();
    }
}
