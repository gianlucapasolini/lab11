﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_JavaToCSharp.robot.components
{
    public interface ICommandableRobotPart : IRobotPart
    {
        void SendCommand(string c);
        string [] AvailableCommands();
    }
}
