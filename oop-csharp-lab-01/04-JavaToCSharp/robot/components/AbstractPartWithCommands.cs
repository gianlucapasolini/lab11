﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_JavaToCSharp.robot.components
{
    public abstract class AbstractPartWithCommands : AbstractPart, ICommandableRobotPart
    {

        protected const string NULL_CMD = "No command";

        public string[] Commands { get; private set; }
        private string SelectedCommand = NULL_CMD;

        protected AbstractPartWithCommands(string desc, double consumption, params string[] cmds)
            : base(desc, consumption)
        {
            cmds.CopyTo(Commands, cmds.Length);
        }

        public string[] AvailableCommands()
        {
            return Commands;
        }

        public override bool DoOperation()
        {
            if (IsPlugged() && IsOn() && !SelectedCommand.Equals(NULL_CMD))
            {
                return DoOperation(SelectedCommand);
            }
            return false;
        }

        protected abstract bool DoOperation(String command);

        public void SendCommand(string c)
        {
            foreach (string s in Commands)
            {
                if (s.Equals(c))
                {
                    SelectedCommand = s;
                }
            }
        }

    }
}
