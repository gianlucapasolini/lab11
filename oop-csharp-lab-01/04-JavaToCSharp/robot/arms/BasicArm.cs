﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _04_JavaToCSharp.robot.arms
{
    public class BasicArm
    {
        private const double ENERGY_REQUIRED_TO_MOVE = 0.2;
        private const double ENERGY_REQUIRED_TO_GRAB = 0.1;
        public bool Grabbing { get; private set; }
        public string Name { get; private set; }

        public BasicArm(string name)
        {
            this.Name = name;
        }
        public void PickUp()
        {
            Grabbing = true;
        }
        public void DropDown()
        {
            Grabbing = false;
        }
        public double getConsuptionForPickUp()
        {
            return ENERGY_REQUIRED_TO_MOVE + ENERGY_REQUIRED_TO_GRAB;
        }
        public double getConsuptionForDropDown()
        {
            return ENERGY_REQUIRED_TO_MOVE;
        }
        public override string ToString()
        {
            return Name;
        }
    }
}
