﻿using System;

namespace ComplexNumbers
{
    class ComplexNum
    {
        public ComplexNum(double re, double im)
        {
            Re = re;
            Im = im;
        }

        public double Re { get; private set; }
        public double Im { get; private set; }


        // Restituisce il modulo del numero complesso
        public double Module
        {
            get
            {
                return Math.Sqrt(Math.Pow(Re, 2) + Math.Pow(Im, 2));
            }
        }

        // Restituisce il complesso coniugato del numero complesso (https://en.wikipedia.org/wiki/Complex_conjugate)
        public ComplexNum Conjugate
        {
            get
            {
                return new ComplexNum(Re, Im * -1);
            }
        }

        public ComplexNum Invert
        {
            get
            {
                return this;
            }
        }

        public static ComplexNum operator +(ComplexNum p1, ComplexNum p2)
        {
            return new ComplexNum(p1.Re + p2.Re, p1.Im + p2.Im);
        }
        public static ComplexNum operator -(ComplexNum p1, ComplexNum p2)
        {
            return new ComplexNum(p1.Re - p2.Re, p1.Im - p2.Im);
        }
        public static ComplexNum operator *(ComplexNum p1, ComplexNum p2)
        {
            return new ComplexNum(p1.Re * p2.Re - p1.Im * p2.Im, p1.Re * p2.Im + p1.Im * p2.Re);
        }
        public static ComplexNum operator /(ComplexNum p1, ComplexNum p2)
        {
            //((ac + bd) / (c2 + d2)) + ((bc - ad) / (c2 + d2)i
            return new ComplexNum(((p1.Re * p2.Re + p1.Im * p2.Im) / (Math.Pow(p2.Re, 2) + Math.Pow(p2.Im, 2))), ((p1.Im * p2.Re - p1.Re * p2.Im) / (Math.Pow(p2.Re, 2) + Math.Pow(p2.Im, 2))));
        }

        // Restituisce una rappresentazione idonea per il numero complesso
        public override string ToString()
        {
            if(Im >= 0)
                return Re + " +" + Im + "i\n";
            else
                return Re + " " + Im + "i\n";
        }
    }
}
