﻿using System;

namespace Cards
{
    class Deck
    {
        private Card[] cards;

        public Deck()
        { 
        }

        public void Initialize()
        {
            /*
             * == README ==
             * 
             * I due enumerati (ItalianSeed e ItalianValue) definiti in fondo a questo file rappresentano rispettivamente semi e valori
             * di un tipo mazzo di carte da gioco italiano.
             * 
             * Questo metodo deve inizializzare il mazzo (l'array cards) considerando tutte le combinazioni possibili.
             * 
             * Nota - Dato un generico enumerato MyEnum:
             *   a) è possibile ottenere il numero dei valori presenti con Enum.GetNames(typeof(MyEnum)).Length
             *   b) è possibile ottenere l'elenco dei valori presenti con (MyEnum[])Enum.GetValues(typeof(MyEnum))
             * 
             */
            this.cards = new Card[Enum.GetNames(typeof(ItalianSeed)).Length * Enum.GetNames(typeof(ItalianValue)).Length];

            int i = 0, j = 0;
            foreach (var seed in (ItalianSeed[])Enum.GetValues(typeof(ItalianSeed)))
            {
                foreach (var name in (ItalianValue[])Enum.GetValues(typeof(ItalianValue)))
                {
                    this.cards[(i * 10) + j] = new Card(name.ToString(), seed.ToString());
                    j++;
                }
                i++;
                j = 0;
            }
        }

        public void Print()
        {
            /*
             * == README  ==
             * 
             * Questo metodo stampa tutte le carte presenti nel mazzo, con una propria rappresentazione a scelta.
             */
            foreach(var carta in this.cards)
            {
                Console.WriteLine(carta);
            }
        }

        public Card this[ItalianSeed seed, ItalianValue name]
        {
            get
            {
                foreach (var carta in this.cards)
                {
                    if (carta.Seed == seed.ToString() && carta.Value == name.ToString())
                        return carta;
                }
                return null;
            }
        }
    }

    enum ItalianSeed
    {
        DENARI,
        COPPE,
        SPADE,
        BASTONI
    }

    enum ItalianValue
    {
        ASSO,
        DUE,
        TRE,
        QUATTRO,
        CINQUE,
        SEI,
        SETTE,
        FANTE,
        CAVALLO,
        RE
    }
}
