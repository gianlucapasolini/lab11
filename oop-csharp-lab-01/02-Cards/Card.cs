﻿namespace Cards
{
    class Card
    {
        public string Seed { get; private set; }
        public string Value { get; private set; }

        public Card(string value, string seed)
        {
            this.Value = value;
            this.Seed = seed;
        }

        public override string ToString()
        {
            // TODO comprendere il meccanismo denominato in C# "string interpolation"
            return $"{this.GetType().Name}(Name={Value}, Seed={Seed})";
        }
    }

    
}
